package controllers

import (
	"fmt"
	"encoding/json"
	"net/http"
	"github.com/gorilla/mux"
	"assignment-5/helpers"
	"assignment-5/models"
	"assignment-5/services"
)

type StudentController struct {
	studentService services.IStudentService
}

func InitStudentController(studentService services.IStudentService) StudentController {
	studentController := new(StudentController)
	studentController.studentService = studentService
	return *studentController
}

func (s *StudentController) Update(res http.ResponseWriter, req *http.Request) {
	var dataBody models.Student
	err := json.NewDecoder(req.Body).Decode(&dataBody)
	if err != nil {
		helpers.ResponseBadRequest(res, http.StatusBadRequest, err, "Bad Request")
		return
	}
	err = s.studentService.Update(dataBody)
	if err != nil {
		helpers.ResponseBadRequest(res, http.StatusBadRequest, err, "Bad Request")
		return
	}
	helpers.Response(res, http.StatusOK, nil, "OK")
}

func (s *StudentController) GetByNPM(res http.ResponseWriter, req *http.Request) {

	npm := mux.Vars(req)["npm"]
	data, err := s.studentService.GetByNPM(npm)
	if err != nil {
		helpers.ResponseBadRequest(res, http.StatusBadRequest, err, "Bad Request")
		return
	}
	helpers.Response(res, http.StatusOK, data, "OK")
}

