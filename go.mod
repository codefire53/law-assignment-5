module assignment-5

go 1.16

require (
	github.com/google/uuid v1.1.2 //
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.7.3
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.10.0
	github.com/magiconair/properties v1.8.5
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2
	google.golang.org/api v0.40.0
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.9
)
