// Package helpers implements commonly used functions (response API)//
package helpers

import (
	"encoding/json"
	"net/http"

)

type APIResponse struct {
	Status string         `json:"status"`
	Data   interface{} `json:"data,omitempty"`
}

type APIResponseError struct {
	Error  string `json:"error"`
	Status string    `json:"status"`
}

func Response(w http.ResponseWriter, httpStatus int, data interface{}, statusMessage string) {
	apiResponse := new(APIResponse)
	apiResponse.Status = statusMessage
	if data != nil {
		apiResponse.Data = data
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpStatus)
	json.NewEncoder(w).Encode(apiResponse)
}

func ResponseBadRequest(w http.ResponseWriter, httpStatus int, err error, statusMessage string) {
	apiResponse := new(APIResponseError)
	apiResponse.Error = err.Error()
	apiResponse.Status = statusMessage

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpStatus)
	json.NewEncoder(w).Encode(apiResponse)
}
