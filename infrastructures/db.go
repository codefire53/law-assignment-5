package infrastructures

import (
	"fmt"
	"assignment-5/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var db *gorm.DB

//InitDB ...
func InitDB() {
	conn, err := dbSetup()
	if err != nil {
		panic(err)
	}
	db = conn
	doMigration()
}

//GetDB ...
func GetDB() *gorm.DB {
	return db
}

//SetDB ...
func SetDB(newDB *gorm.DB) {
	db = newDB
}

func doMigration() {
	db.AutoMigrate(&models.Student{})
}

func dbSetup() (*gorm.DB, error) {
	username := "postgres"
	password := "postgres"
	dbName := "postgres"
	host := "localhost"
	dbParams := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable host=%s", username, password, dbName, host)
	conn, err := gorm.Open(postgres.New(postgres.Config{
		DSN: dbParams, // data source name, refer https://github.com/jackc/pgx
		PreferSimpleProtocol: true,                                     // disables implicit prepared statement usage. By default pgx automatically uses the extended protocol
	}), &gorm.Config{})
	return conn, err
}
