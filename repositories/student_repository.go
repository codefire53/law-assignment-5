package repositories

import (
	"assignment-5/infrastructures"
	"assignment-5/models"
)

type IStudentRepository interface {
	Update(student models.Student) (error)
	GetByNPM(npm string) (models.Student, error)
}
type StudentRepository struct {
}

func (s StudentRepository) Update(student models.Student) ( err error) {
	db := infrastructures.GetDB()
	err = nil
	if db.Model(&student).Where("npm = ?", student.NPM).Updates(&student).RowsAffected == 0 {
		err = db.Create(&student).Error
	}
	return
}

func (s StudentRepository) GetByNPM(npm string) (selectedStudent models.Student, err error) {
	db := infrastructures.GetDB()
	err = db.Where("npm = ?", npm).First(&selectedStudent).Error
	return 
}
