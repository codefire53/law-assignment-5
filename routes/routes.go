package routes

import (
	"github.com/gorilla/mux"
	"assignment-5/controllers"
	"assignment-5/repositories"
	"assignment-5/services"
)

// Route is
type Route struct{}

// Init is the initiator for this project
func (r *Route) Init() *mux.Router {

	// init repositories
	studentRepository := new(repositories.StudentRepository)

	// init services
	studentService := services.InitStudentService(studentRepository)

	// init Controllers
	studentController := controllers.InitStudentController(studentService)

	// init routes
	router := mux.NewRouter().StrictSlash(false)
	student := router.PathPrefix("/").Subrouter()
	student.HandleFunc("/update", studentController.Update).Methods("POST")
	student.HandleFunc("/read/{npm}", studentController.GetByNPM).Methods("GET")
	return router
}

