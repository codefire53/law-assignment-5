package services

import (
	"fmt"
	"assignment-5/models"
	"assignment-5/repositories"
)

type IStudentService interface {
	Update(student models.Student) (error)
	GetByNPM(npm string) (models.Student, error)
}

type StudentService struct {
	studentRepository repositories.IStudentRepository
}

func InitStudentService(studentRepository repositories.IStudentRepository) IStudentService {
	studentService := new(StudentService)
	studentService.studentRepository = studentRepository
	return studentService
}

func (s StudentService) Update(student models.Student) (err error) {
	err = s.studentRepository.Update(student)
	return
}

func (s StudentService) GetByNPM(npm string) (targetStudent models.Student, err error) {
	targetStudent, err = s.studentRepository.GetByNPM(npm)
	return 
}
